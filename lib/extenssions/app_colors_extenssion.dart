import 'package:flutter/material.dart';

class AppColors {
  static const Color app_primary = Color(0xFF0D0E56);
  static const Color app_primary_light = Color(0xFFF1F4FF);
  static const Color app_button_color = Color(0xFF472FC8);
  static const Color app_button_color_disabled = Color(0xFFB1B1BE);
  static const Color text_field_hint_color = Color(0xFF7B7C98);
  static const Color text_button_color = Color(0xFF351DB6);
  static const Color text_field_title_color = Color(0xFF181819);
  static const Color shadow_color = Color(0xFFE9E7F1);
  static const Color red = Color(0xFFD50000);
  static const Color green = Color(0xFF00BEA1);
  static const Color un_selected_item_color = Color(0xFFD3CFEA);
  static const Color category_actions_color = Color(0xFF9F9DED);
  static const Color tip_date_color = Color(0xFFBABAD7);
  static const Color action_date_color = Color(0xFFB9BACE);
}