
import 'package:budget_planner/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserPreferences {
  static final UserPreferences _instance = UserPreferences._();
  late SharedPreferences _sharedPreferences;

  factory UserPreferences(){
    return _instance;
  }

  UserPreferences._();

  Future<void> initPreferences() async {
    _sharedPreferences = await SharedPreferences.getInstance();
  }

  Future<void> save({required User user}) async {
    await _sharedPreferences.setBool('logged_in', true);
    await _sharedPreferences.setInt('id', user.id);
    await _sharedPreferences.setString('name', user.name);
    await _sharedPreferences.setString('email', user.email);
    await _sharedPreferences.setInt('currencyId', user.currencyId);
    await _sharedPreferences.setDouble('dayLimit', user.dayLimit);
    await _sharedPreferences.setInt('pin', user.pin);
  }

  User get user {
    User user = User();
    user.id = _sharedPreferences.getInt('id') ?? 0;
    user.name = _sharedPreferences.getString('name') ?? '';
    user.email = _sharedPreferences.getString('email') ?? '';
    user.currencyId = _sharedPreferences.getInt('currencyId') ?? 0;
    user.dayLimit = _sharedPreferences.getDouble('dayLimit') ?? 0;
    user.pin = _sharedPreferences.getInt('pin') ?? 0;
    return user;
  }

  bool get loggedIn => _sharedPreferences.getBool('logged_in') ?? false;

  Future<bool> logout() async {
    return await _sharedPreferences.clear();
  }
}