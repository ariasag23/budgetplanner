import 'package:budget_planner/getx_controllers/users_getx_controller.dart';
import 'package:budget_planner/models/category.dart';
import 'package:budget_planner/storage/db_operations.dart';
import 'package:budget_planner/storage/db_provider.dart';
import 'package:sqflite/sqflite.dart';

class CategoriesDbController extends DbOperations<Category> {
  Database _database;

  CategoriesDbController() : _database = DBProvider().database;

  @override
  Future<int> create(Category data) {
    // TODO: implement create
    return _database.insert('categories', data.toMap());
  }

  @override
  Future<bool> update(Category data) {
    // TODO: implement update
    throw UnimplementedError();
  }

  @override
  Future<List<Category>> read() async {
    // TODO: implement read
    List<Map<String, dynamic>> data = await _database.query('categories',
        where: 'user_id = ?', whereArgs: [UsersGetxController.to.user.id]);
    if (data.isNotEmpty) {
      return data.map((rowMap) => Category.fromMap(rowMap)).toList();
    }
    return [];
  }

  @override
  Future<bool> delete(int id) {
    // TODO: implement delete
    throw UnimplementedError();
  }

  @override
  Future<Category?> show(int id) {
    // TODO: implement show
    throw UnimplementedError();
  }

  Future<bool> deleteUserCategories(int userId) async {
    int deleteRowsCount = await _database
        .delete('categories', where: 'user_id = ?', whereArgs: [userId]);
    return deleteRowsCount > 0;
  }
}
