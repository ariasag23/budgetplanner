import 'package:budget_planner/extenssions/app_colors_extenssion.dart';
import 'package:budget_planner/preferences/user_preferences.dart';
import 'package:budget_planner/screens/app/actions/add_action_screen.dart';
import 'package:budget_planner/screens/app/settings/about_screen.dart';
import 'package:budget_planner/screens/app/settings/settings_screen.dart';
import 'package:budget_planner/screens/auth/create_account_screen.dart';
import 'package:budget_planner/screens/auth/pin_code_screen.dart';
import 'package:budget_planner/screens/bottom_navigation/categories_screen.dart';
import 'package:budget_planner/screens/category/category_screen.dart';
import 'package:budget_planner/screens/currencies/currency_screen.dart';
import 'package:budget_planner/screens/launch_screen.dart';
import 'package:budget_planner/screens/auth/login_screen.dart';
import 'package:budget_planner/screens/app/main_screen.dart';
import 'package:budget_planner/screens/out_boarding/out_boarding_screen.dart';
import 'package:budget_planner/screens/success_screen.dart';
import 'package:budget_planner/storage/db_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await DBProvider().initDatabase();
  await UserPreferences().initPreferences();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        fontFamily: 'Montserrat',
        appBarTheme: AppBarTheme(
          elevation: 0,
          color: Colors.white,
          iconTheme: IconThemeData(
            color: AppColors.app_button_color,
          ),
          textTheme: TextTheme(
            headline6: TextStyle(
              color: AppColors.app_primary,
              fontFamily: 'Montserrat',
              fontWeight: FontWeight.bold,
              fontSize: 20
            ),
          ),
        ),
        textButtonTheme: TextButtonThemeData(
          style: ButtonStyle(
            foregroundColor: MaterialStateProperty.resolveWith(
              (states) => AppColors.text_button_color,
            ),
            textStyle: MaterialStateProperty.all(
              TextStyle(
                fontFamily: 'Montserrat',
              ),
            ),
          ),
        ),
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ButtonStyle(
            textStyle: MaterialStateProperty.all(
              TextStyle(
                fontFamily: 'Montserrat',
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
        textTheme: TextTheme(
          bodyText2: TextStyle(
            fontFamily: 'Montserrat',
            color: AppColors.app_primary,
          ),
        ),
      ),
      localizationsDelegates: [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en'), // English, no country code
        const Locale('ar'), // Spanish, no country code
      ],
      locale: Locale('en'),
      initialRoute: '/launch_screen',
      routes: {
        '/launch_screen': (context) => LaunchScreen(),
        '/out_boarding_screen': (context) => OutBoardingScreen(),
        '/login_screen': (context) => LoginScreen(),
        '/create_account_screen': (context) => CreateAccountScreen(),
        '/pin_code_screen': (context) => PinCodeScreen(),
        '/currency_screen': (context) => CurrencyScreen(),
        '/success_screen': (context) => SuccessScreen(),
        '/main_screen': (context) => MainScreen(),
        '/category_screen': (context) => CategoryScreen(),
        '/add_action_screen': (context) => AddActionScreen(),
        '/settings_screen': (context) => SettingsScreen(),
        '/about_screen': (context) => AboutScreen(),
      },
    );
  }
}
