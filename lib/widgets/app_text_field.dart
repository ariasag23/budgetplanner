import 'package:budget_planner/extenssions/app_colors_extenssion.dart';
import 'package:budget_planner/utils/size_config.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class AppTextField extends StatelessWidget {
  final String hintText;
  final TextInputType keyboardType;
  final TextEditingController controller;
  final int maxLength;
  final int maxLines;
  final TextAlign textAlign;
  final bool enablePadding;
  final IconData? prefixIcon;
  final void Function(String value)? onChanged;

  AppTextField({
    required this.controller,
    required this.hintText,
    this.textAlign = TextAlign.end,
    this.maxLength = 20,
    this.maxLines = 1,
    this.enablePadding = false,
    this.onChanged,
    this.prefixIcon,
    this.keyboardType = TextInputType.text,
  });

  @override
  Widget build(BuildContext context) {
    return TextField(
      textAlign: textAlign,
      keyboardType: keyboardType,
      controller: controller,
      maxLength: maxLength,
      maxLines: maxLines,
      textAlignVertical: TextAlignVertical.center,
      style: TextStyle(
        color: AppColors.text_field_hint_color,
        fontFamily: 'Montserrat',
        fontSize: SizeConfig().scaleTextFont(15),
      ),
      onChanged: onChanged,
      decoration: InputDecoration(
        contentPadding:enablePadding ? EdgeInsetsDirectional.only(
          start: SizeConfig().scaleWidth(15),
          end: SizeConfig().scaleWidth(15),
          top: SizeConfig().scaleHeight(22),
          bottom: SizeConfig().scaleHeight(22),
        ) : null,
        hintText: hintText,
        counterText: '',
        hintStyle: TextStyle(
          color: AppColors.text_field_hint_color,
          fontFamily: 'Montserrat',
          fontSize: SizeConfig().scaleTextFont(15),
        ),
        prefixIcon: prefixIcon != null ? Icon(prefixIcon) : null,
        enabledBorder: InputBorder.none,
        focusedBorder: InputBorder.none,
      ),
    );
  }
}
