import 'package:budget_planner/extenssions/app_colors_extenssion.dart';
import 'package:budget_planner/utils/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AppElevatedButton extends StatelessWidget {
  final String title;
  final void Function() onPressed;
  final Color color;

  AppElevatedButton({
    required this.title,
    required this.onPressed,
    this.color = AppColors.app_button_color,
  });

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      child: Text(title),
      style: ElevatedButton.styleFrom(
        primary: color,
        minimumSize: Size(
          double.infinity,
          SizeConfig().scaleHeight(60),
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30),
        ),
      ),
    );
  }
}
