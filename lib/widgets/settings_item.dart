import 'package:budget_planner/extenssions/app_colors_extenssion.dart';
import 'package:budget_planner/utils/size_config.dart';
import 'package:flutter/material.dart';

class SettingsItem extends StatelessWidget {
  final String title;
  final IconData icon;
  final Color iconColor;
  final Color textColor;
  final Color backgroundColor;
  final void Function() onPressed;
  final double marginBottom;
  final double marginTop;
  final Color borderColor;

  SettingsItem({
    required this.title,
    required this.icon,
    required this.onPressed,
    this.iconColor = AppColors.app_primary,
    this.textColor = AppColors.app_primary,
    this.backgroundColor = Colors.white,
    this.marginBottom = 15,
    this.marginTop = 0,
    this.borderColor = Colors.transparent,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        bottom: SizeConfig().scaleHeight(marginBottom),
        top: SizeConfig().scaleHeight(marginTop),
      ),
      height: SizeConfig().scaleHeight(70),
      padding: EdgeInsets.symmetric(
        horizontal: SizeConfig().scaleWidth(16),
        vertical: SizeConfig().scaleHeight(23),
      ),
      decoration: BoxDecoration(
        color: backgroundColor,
        borderRadius: BorderRadius.circular(8),
        border: Border.all(
            color: borderColor,
            width: borderColor == Colors.transparent ? 0 : 1),
        boxShadow: [
          BoxShadow(
            offset: Offset(0, 0),
            color: Color(0xFF8F8F8F).withOpacity(0.4),
            blurRadius: 7,
          ),
        ],
      ),
      child: InkWell(
        onTap: onPressed,
        child: Row(
          children: [
            Icon(icon, color: iconColor),
            SizedBox(width: SizeConfig().scaleWidth(10)),
            Text(
              title,
              style: TextStyle(
                color: textColor,
                fontWeight: FontWeight.w500,
                fontSize: SizeConfig().scaleTextFont(15),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
