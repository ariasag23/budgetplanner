import 'package:budget_planner/extenssions/app_colors_extenssion.dart';
import 'package:budget_planner/utils/size_config.dart';
import 'package:flutter/material.dart';

class CategoryItem extends StatelessWidget {
  final String title;
  final String? actionsCount;
  final void Function()? onTap;
  final bool? checked;

  CategoryItem({
    required this.title,
    this.checked,
    this.actionsCount,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Icon(
            Icons.category_outlined,
            color: AppColors.text_field_title_color,
          ),
          SizedBox(width: SizeConfig().scaleWidth(13)),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                title,
                style: TextStyle(
                  color: AppColors.text_field_title_color,
                  fontSize: SizeConfig().scaleTextFont(15),
                  fontWeight: FontWeight.w500,
                  fontFamily: 'Montserrat',
                ),
              ),
              Visibility(
                visible: actionsCount != null,
                child: Text(
                  '$actionsCount Action',
                  style: TextStyle(
                    color: AppColors.category_actions_color,
                    fontSize: SizeConfig().scaleTextFont(11),
                    fontWeight: FontWeight.w500,
                    fontFamily: 'Montserrat',
                  ),
                ),
              ),
            ],
          ),
          if (checked != null) Spacer(),
          if (checked != null)
            Icon(
              Icons.check,
              color: checked! ? AppColors.app_button_color : Colors.transparent,
            ),
        ],
      ),
    );
  }
}
