import 'package:budget_planner/extenssions/app_colors_extenssion.dart';
import 'package:budget_planner/models/user_action.dart';
import 'package:budget_planner/utils/size_config.dart';
import 'package:flutter/material.dart';

class ActionItem extends StatelessWidget {
  final UserAction action;

  ActionItem({
    required this.action,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          margin: EdgeInsetsDirectional.only(
            end: SizeConfig().scaleWidth(11),
          ),
          height: SizeConfig().scaleHeight(54),
          width: SizeConfig().scaleWidth(54),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16),
            color: AppColors.app_primary_light,
          ),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              action.category.name,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: SizeConfig().scaleTextFont(15),
                color: AppColors.app_primary,
              ),
            ),
            Visibility(
              visible: action.notes.isNotEmpty,
              child: Text(
                action.notes,
                style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: SizeConfig().scaleTextFont(15),
                  color: AppColors.text_field_hint_color,
                ),
              ),
            )
          ],
        ),
        Spacer(),
        Text(
          amount,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: SizeConfig().scaleTextFont(15),
            color: action.expense ? AppColors.red : AppColors.green,
          ),
        ),
      ],
    );
  }

  String get amount {
    String currencySymbol = getCurrencySymbol();
    return action.expense
        ? '- $currencySymbol ${action.amount}'
        : '+ $currencySymbol ${action.amount}';
  }

  String getCurrencySymbol() {
    if (action.currencyId == 1) {
      return '₪';
    } else if (action.currencyId == 2) {
      return '\$';
    } else {
      return 'JOD';
    }
  }
}
