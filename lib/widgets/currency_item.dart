import 'package:budget_planner/extenssions/app_colors_extenssion.dart';
import 'package:flutter/material.dart';

class CurrencyItem extends StatelessWidget {
  final String name;
  final bool checked;
  final void Function() onPressed;

  CurrencyItem({
    required this.name,
    required this.checked,
    required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          name,
          style: TextStyle(
            color: AppColors.app_primary,
          ),
        ),
        Spacer(),
        IconButton(
          onPressed: onPressed,
          icon: Icon(Icons.check),
          color: checked ? AppColors.app_button_color : Colors.transparent,
        ),
      ],
    );
  }
}
