import 'package:budget_planner/utils/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AppIcon extends StatelessWidget {
  const AppIcon({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: SizeConfig().scaleWidth(170),
      height: SizeConfig().scaleHeight(167),
      margin: EdgeInsets.only(
        bottom: SizeConfig().scaleHeight(29),
      ),
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(45),
        boxShadow: [
          BoxShadow(
            offset: Offset(0, 4),
            color: Colors.black.withOpacity(0.25),
            blurRadius: 4,
            spreadRadius: 0,
          ),
        ],
      ),
      child: SvgPicture.asset(
        'images/app_icon.svg',
      ),
    );
  }
}
