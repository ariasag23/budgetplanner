import 'package:budget_planner/extenssions/app_colors_extenssion.dart';
import 'package:budget_planner/getx_controllers/category_getx_controller.dart';
import 'package:budget_planner/models/category.dart';
import 'package:budget_planner/utils/size_config.dart';
import 'package:budget_planner/widgets/category_item.dart';
import 'package:flutter/material.dart';

class CategoryTabContent extends StatelessWidget {
  final List<Category> categories;
  final bool hideActionsCount;

  CategoryTabContent({
    required this.categories,
    this.hideActionsCount = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsetsDirectional.only(
        start: SizeConfig().scaleWidth(20),
        end: SizeConfig().scaleWidth(20),
        bottom: SizeConfig().scaleHeight(20),
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [
          BoxShadow(
            offset: Offset(0, 10),
            color: AppColors.shadow_color,
            blurRadius: 15,
          ),
        ],
      ),
      child: ListView.separated(
        clipBehavior: Clip.antiAlias,
        padding: EdgeInsetsDirectional.only(
          top: SizeConfig().scaleHeight(22),
          bottom: SizeConfig().scaleHeight(22),
          start: SizeConfig().scaleWidth(15),
          end: SizeConfig().scaleWidth(15),
        ),
        itemBuilder: (context, index) {
          return CategoryItem(
            onTap: () {
              print('SELECT CATEGORY');
              CategoryGetxController.to.changeCheckStatus(categories[index].id);
            },
            title: categories[index].name,
            actionsCount: hideActionsCount ? null : '',
            checked:  categories[index].checked,
          );
        },
        separatorBuilder: (context, index) {
          return Divider(
            height: SizeConfig().scaleHeight(48),
            color: AppColors.text_field_hint_color.withOpacity(0.2),
            thickness: SizeConfig().scaleWidth(1),
          );
        },
        itemCount: categories.length,
      ),
    );
  }
}
