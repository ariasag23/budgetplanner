import 'package:budget_planner/extenssions/app_colors_extenssion.dart';
import 'package:budget_planner/utils/size_config.dart';
import 'package:flutter/material.dart';

class AddActionButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: SizeConfig().scaleHeight(45),
      width: SizeConfig().scaleWidth(45),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: AppColors.app_button_color,
      ),
      child: IconButton(
        onPressed: () => Navigator.pushNamed(context, '/add_action_screen'),
        padding: EdgeInsets.zero,
        icon: Icon(Icons.add),
        iconSize: SizeConfig().scaleWidth(30),
        color: Colors.white,
      ),
    );
  }
}
