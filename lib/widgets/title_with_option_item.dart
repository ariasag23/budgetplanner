import 'package:budget_planner/extenssions/app_colors_extenssion.dart';
import 'package:budget_planner/utils/size_config.dart';
import 'package:flutter/material.dart';

class TitleWithOptionItem extends StatelessWidget {
  final String title;
  final String? content;
  final void Function() onPressed;

  TitleWithOptionItem({
    required this.title,
    this.content,
    required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          title,
          style: TextStyle(
            color: AppColors.text_field_title_color,
            fontWeight: FontWeight.w500,
            fontFamily: 'Montserrat',
            fontSize: SizeConfig().scaleTextFont(15),
          ),
        ),
        Expanded(
          child: TextButton(
            onPressed: onPressed,
            style: ButtonStyle(
                alignment: AlignmentDirectional.centerEnd,
                overlayColor: MaterialStateColor.resolveWith(
                    (states) => Colors.transparent)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(
                  content ?? 'None',
                  style: TextStyle(
                    color: AppColors.text_field_hint_color,
                    fontFamily: 'Montserrat',
                    fontSize: SizeConfig().scaleTextFont(15),
                  ),
                ),
                Icon(
                  Icons.arrow_forward_ios,
                  size: 15,
                  color: AppColors.text_field_hint_color,
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}
