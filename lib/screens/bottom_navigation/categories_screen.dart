import 'package:budget_planner/extenssions/app_colors_extenssion.dart';
import 'package:budget_planner/getx_controllers/category_getx_controller.dart';
import 'package:budget_planner/utils/enums.dart';
import 'package:budget_planner/utils/size_config.dart';
import 'package:budget_planner/widgets/category_tab_content.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CategoriesScreen extends StatefulWidget {
  final bool withScaffold;

  CategoriesScreen({this.withScaffold = false});

  @override
  _CategoriesScreenState createState() => _CategoriesScreenState();
}

class _CategoriesScreenState extends State<CategoriesScreen>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  CategoryGetxController _categoryGetxController =
      Get.put(CategoryGetxController());

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return widget.withScaffold
        ? Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              backgroundColor: Colors.white,
              elevation: 0,
              title: Text('Category'),
              leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () => popFromCategory(),
              ),
              actions: [
                IconButton(
                  onPressed: () =>
                      Navigator.pushNamed(context, '/category_screen'),
                  icon: Icon(Icons.add_circle_outlined),
                ),
              ],
            ),
            body: screenContent(),
          )
        : screenContent();
  }

  Widget screenContent() {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.symmetric(
            horizontal: SizeConfig().scaleWidth(20),
            vertical: SizeConfig().scaleHeight(20),
          ),
          height: SizeConfig().scaleHeight(38),
          decoration: BoxDecoration(
            color: AppColors.app_primary_light,
            borderRadius: BorderRadius.circular(19),
          ),
          child: TabBar(
            onTap: (int selectedTab) {},
            controller: _tabController,
            indicator: BoxDecoration(
              borderRadius: BorderRadius.circular(19),
              color: AppColors.app_button_color,
            ),
            labelColor: Colors.white,
            unselectedLabelColor: Colors.black,
            labelStyle: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: SizeConfig().scaleTextFont(13),
            ),
            unselectedLabelStyle: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: SizeConfig().scaleTextFont(13),
            ),
            tabs: [
              Tab(text: 'Expenses'),
              Tab(text: 'Income'),
            ],
          ),
        ),
        GetX<CategoryGetxController>(
          tag: 'CategoryGetxController',
          builder: (CategoryGetxController controller) {
            return Expanded(
              child: TabBarView(
                controller: _tabController,
                children: [
                  CategoryTabContent(
                    hideActionsCount: true,
                    categories: controller.getCategoriesByType(
                      type: CategoryType.Expense,
                    ),
                  ),
                  CategoryTabContent(
                    hideActionsCount: true,
                    categories: controller.getCategoriesByType(
                      type: CategoryType.Income,
                    ),
                  ),
                ],
              ),
            );
          },
        )
      ],
    );
  }

  void popFromCategory() {
    Navigator.pop(context, CategoryGetxController.to.getSelectedCategory());
  }
}
