import 'package:budget_planner/extenssions/app_colors_extenssion.dart';
import 'package:budget_planner/getx_controllers/currencies_getx_controller.dart';
import 'package:budget_planner/getx_controllers/users_getx_controller.dart';
import 'package:budget_planner/models/currency.dart';
import 'package:budget_planner/models/user.dart';
import 'package:budget_planner/utils/size_config.dart';
import 'package:budget_planner/widgets/app_elevated_button.dart';
import 'package:budget_planner/widgets/app_text_field.dart';
import 'package:budget_planner/widgets/screen_header.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  String _pinCode = '';
  Currency? _currency;
  bool _createdEnabled = false;

  late TextEditingController _nameTextController;
  late TextEditingController _emailTextController;
  late TextEditingController _dailyLimitTextController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    User user = UsersGetxController.to.user;
    _nameTextController = TextEditingController(text: user.name);
    _emailTextController = TextEditingController(text: user.email);
    _dailyLimitTextController =
        TextEditingController(text: user.dayLimit.toString());
    _currency = CurrencyGetxController.to
        .getCurrencyById(id: user.currencyId, setSelected: true);
    _pinCode = user.pin.toString();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _nameTextController.dispose();
    _emailTextController.dispose();
    _dailyLimitTextController.dispose();
    CurrencyGetxController.to.undoCheckedCurrency();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsetsDirectional.only(
        start: SizeConfig().scaleWidth(20),
        end: SizeConfig().scaleWidth(20),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ScreenHeader(
            imageName: 'icon_wallet',
            title: UsersGetxController.to.user.name,
            info: '',
          ),
          SizedBox(height: SizeConfig().scaleHeight(0)),
          Container(
            margin: EdgeInsets.only(bottom: SizeConfig().scaleHeight(25)),
            padding: EdgeInsets.symmetric(
              horizontal: SizeConfig().scaleWidth(17),
              vertical: SizeConfig().scaleHeight(10),
            ),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(8),
              boxShadow: [
                BoxShadow(
                  offset: Offset(0, 3),
                  color: Colors.black.withOpacity(0.25),
                  blurRadius: 6,
                ),
              ],
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      'Name:',
                      style: TextStyle(
                        color: AppColors.text_field_title_color,
                        fontFamily: 'Montserrat',
                        fontSize: SizeConfig().scaleTextFont(15),
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    Expanded(
                      child: AppTextField(
                        controller: _nameTextController,
                        hintText: 'None',
                        maxLength: 25,
                        keyboardType: TextInputType.text,
                        onChanged: (String value) => validateForm(),
                      ),
                    ),
                  ],
                ),
                Divider(
                  thickness: 0.5,
                  color: AppColors.text_field_hint_color,
                ),
                Row(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.email_title,
                      style: TextStyle(
                        color: AppColors.text_field_title_color,
                        fontWeight: FontWeight.w500,
                        fontFamily: 'Montserrat',
                        fontSize: SizeConfig().scaleTextFont(15),
                      ),
                    ),
                    Expanded(
                      child: AppTextField(
                          controller: _emailTextController,
                          hintText: 'None',
                          maxLength: 25,
                          keyboardType: TextInputType.emailAddress,
                          onChanged: (String value) => validateForm()),
                    )
                  ],
                ),
                Divider(
                  thickness: 0.5,
                  color: AppColors.text_field_hint_color,
                ),
                Row(
                  children: [
                    Text(
                      'Currency',
                      style: TextStyle(
                        color: AppColors.text_field_title_color,
                        fontWeight: FontWeight.w500,
                        fontFamily: 'Montserrat',
                        fontSize: SizeConfig().scaleTextFont(15),
                      ),
                    ),
                    Expanded(
                      child: TextButton(
                        onPressed: () => navigateToCurrencyScreen(),
                        style: ButtonStyle(
                            alignment: AlignmentDirectional.centerEnd,
                            overlayColor: MaterialStateColor.resolveWith(
                                (states) => Colors.transparent)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              _currency?.nameEn ?? 'None',
                              style: TextStyle(
                                color: AppColors.text_field_hint_color,
                                fontFamily: 'Montserrat',
                                fontSize: SizeConfig().scaleTextFont(15),
                              ),
                            ),
                            Icon(
                              Icons.arrow_forward_ios,
                              size: 15,
                              color: AppColors.text_field_hint_color,
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
                Divider(
                  thickness: 0.5,
                  color: AppColors.text_field_hint_color,
                ),
                Row(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.daily_limit,
                      style: TextStyle(
                        color: AppColors.text_field_title_color,
                        fontWeight: FontWeight.w500,
                        fontFamily: 'Montserrat',
                        fontSize: SizeConfig().scaleTextFont(15),
                      ),
                    ),
                    Expanded(
                      child: AppTextField(
                        controller: _dailyLimitTextController,
                        hintText: '\$ 0',
                        maxLength: 7,
                        keyboardType: TextInputType.number,
                        onChanged: (String value) => validateForm(),
                      ),
                    )
                  ],
                ),
                Divider(
                  thickness: 0.5,
                  color: AppColors.text_field_hint_color,
                ),
                Row(
                  children: [
                    TextButton(
                      onPressed: () => navigateToPinCodeScreen(),
                      child: Text('Set your pin'),
                      style: TextButton.styleFrom(
                          padding: EdgeInsets.zero,
                          primary: AppColors.text_field_title_color),
                    ),
                    Spacer(),
                    Text(
                      _pinCode,
                      style: TextStyle(
                        color: AppColors.text_field_hint_color,
                        fontFamily: 'Montserrat',
                        fontSize: SizeConfig().scaleTextFont(15),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
          AppElevatedButton(
            title: AppLocalizations.of(context)!.save,
            onPressed: () async {
              await updateProfile();
            },
            color: _createdEnabled
                ? AppColors.app_button_color
                : AppColors.app_button_color_disabled,
          ),
        ],
      ),
    );
  }

  void navigateToCurrencyScreen() async {
    var currency =
        await Navigator.pushNamed(context, '/currency_screen') as Currency?;
    if (currency != null) {
      setState(() {
        _currency = currency;
      });
      validateForm();
    }
  }

  void navigateToPinCodeScreen() async {
    var pinCode =
        await Navigator.pushNamed(context, '/pin_code_screen') as String?;
    if (pinCode != null) {
      setState(() {
        _pinCode = pinCode;
      });
      validateForm();
    }
  }

  void validateForm() {
    updateEnableStatus(checkData());
  }

  void updateEnableStatus(bool status) {
    setState(() {
      _createdEnabled = status;
    });
  }

  bool checkData() {
    return _nameTextController.text.isNotEmpty &&
        _emailTextController.text.isNotEmpty &&
        _currency != null &&
        _dailyLimitTextController.text.isNotEmpty &&
        _pinCode.isNotEmpty;
  }

  void performUpdateProfile() async {
    if (_createdEnabled) {
      await updateProfile();
    } else {
      //SHOW ERROR MESSAGE
    }
  }

  Future<void> updateProfile() async {
    bool status = await UsersGetxController.to.createAccount(user: user);
    if (status) {
      CurrencyGetxController.to.undoCheckedCurrency();
      Navigator.pushReplacementNamed(context, '/success_screen');
    } else {
      //SHOW MESSAGE - ERROR
    }
  }

  User get user {
    User user = UsersGetxController.to.user;
    user.name = _nameTextController.text;
    user.email = _emailTextController.text;
    user.pin = int.parse(_pinCode);
    user.dayLimit = double.parse(_dailyLimitTextController.text);
    user.currencyId = _currency!.id;
    return user;
  }
}
