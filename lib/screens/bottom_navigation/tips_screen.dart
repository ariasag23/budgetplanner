import 'package:budget_planner/extenssions/app_colors_extenssion.dart';
import 'package:budget_planner/utils/size_config.dart';
import 'package:budget_planner/widgets/app_text_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class TipsScreen extends StatefulWidget {
  const TipsScreen({Key? key}) : super(key: key);

  @override
  _TipsScreenState createState() => _TipsScreenState();
}

class _TipsScreenState extends State<TipsScreen> {
  late TextEditingController _searchTextController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _searchTextController = TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _searchTextController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: SizeConfig().scaleWidth(20),
      ),
      child: Column(
        children: [
          DecoratedBox(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(8),
            ),
            child: AppTextField(
              textAlign: TextAlign.start,
              controller: _searchTextController,
              hintText: 'Search',
              prefixIcon: Icons.search,
            ),
          ),
          SizedBox(height: SizeConfig().scaleHeight(7)),
          Expanded(
            child: GridView.builder(
              padding: EdgeInsets.symmetric(
                vertical: SizeConfig().scaleHeight(20)
              ),
              itemCount: 5,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                crossAxisSpacing: SizeConfig().scaleWidth(10),
                mainAxisSpacing: SizeConfig().scaleHeight(17),
                childAspectRatio: SizeConfig().scaleWidth(182) /
                    SizeConfig().scaleHeight(230),
              ),
              itemBuilder: (context, index) {
                return Card(
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8)),
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: SizeConfig().scaleWidth(12),
                    ),
                    child: Column(
                      children: [
                        SvgPicture.asset(
                          'images/out_boarding_2.svg',
                          height: SizeConfig().scaleHeight(157),
                        ),
                        Text(
                          'How to save a budget 10 tips',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: AppColors.app_primary,
                            fontWeight: FontWeight.bold,
                            fontSize: SizeConfig().scaleTextFont(15),
                          ),
                        ),
                        SizedBox(height: SizeConfig().scaleHeight(5)),
                        Text(
                          '3 month ago',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: AppColors.tip_date_color,
                            fontSize: SizeConfig().scaleTextFont(13),
                          ),
                        )
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
