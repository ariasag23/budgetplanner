import 'package:budget_planner/extenssions/app_colors_extenssion.dart';
import 'package:budget_planner/getx_controllers/actions_getx_controller.dart';
import 'package:budget_planner/getx_controllers/users_getx_controller.dart';
import 'package:budget_planner/utils/size_config.dart';
import 'package:budget_planner/widgets/action_item.dart';
import 'package:budget_planner/widgets/actions_date_text.dart';
import 'package:budget_planner/widgets/app_elevated_button.dart';
import 'package:budget_planner/widgets/circular_progress_info.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return GetX<ActionsGetxController>(
      tag: 'ActionsGetxController',
      builder: (ActionsGetxController controller) {
        return Padding(
          padding: EdgeInsetsDirectional.only(
            top: SizeConfig().scaleHeight(43.5),
            bottom: SizeConfig().scaleHeight(30),
            start: SizeConfig().scaleWidth(20),
            end: SizeConfig().scaleWidth(20),
          ),
          child: Column(
            children: [
              Container(
                height: SizeConfig().scaleHeight(260),
                width: SizeConfig().scaleWidth(260),
                decoration: BoxDecoration(
                  color: Colors.white,
                  shape: BoxShape.circle,
                  boxShadow: [
                    BoxShadow(
                      color: AppColors.app_button_color.withAlpha(48),
                      offset: Offset(0, 13),
                      blurRadius: 11,
                      spreadRadius: 0,
                    )
                  ],
                ),
                child: CircularPercentIndicator(
                  radius: SizeConfig().scaleHeight(260),
                  lineWidth: 11.0,
                  percent: controller.totalExpenses.value /
                      UsersGetxController.to.user.dayLimit,
                  backgroundColor: AppColors.app_button_color.withOpacity(0.1),
                  progressColor: AppColors.app_button_color,
                  circularStrokeCap: CircularStrokeCap.round,
                  center: CircularProgressInfo(
                    expenses: controller.totalExpenses.value,
                    balance: controller.totalIncomes.value,
                  ),
                ),
              ),
              SizedBox(height: SizeConfig().scaleHeight(60)),
              Align(
                alignment: AlignmentDirectional.centerStart,
                child: Text(
                  'Last actions',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: SizeConfig().scaleTextFont(20),
                  ),
                ),
              ),
              SizedBox(height: SizeConfig().scaleHeight(21)),
              Expanded(
                child: Container(
                  padding: EdgeInsetsDirectional.only(
                    top: SizeConfig().scaleHeight(19),
                    bottom: SizeConfig().scaleHeight(27),
                    start: SizeConfig().scaleWidth(12),
                    end: SizeConfig().scaleWidth(12),
                  ),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8),
                    boxShadow: [
                      BoxShadow(
                        color: AppColors.app_button_color.withAlpha(48),
                        offset: Offset(0, 5),
                        blurRadius: 11,
                        spreadRadius: 0,
                      ),
                    ],
                  ),
                  child: Column(
                    children: [
                      Expanded(
                        child: ListView.separated(
                          separatorBuilder: (context, index) {
                            return Divider(
                              height: SizeConfig().scaleHeight(24),
                            );
                          },
                          itemCount: controller.actions.length,
                          itemBuilder: (context, index) {
                            return Column(
                              children: [
                                index == 0
                                    ? Column(
                                        children: [
                                          ActionDateText(
                                            date:
                                                controller.actions[index].date,
                                          ),
                                          SizedBox(
                                            height:
                                                SizeConfig().scaleHeight(20),
                                          ),
                                          ActionItem(
                                            action: controller.actions[index],
                                          ),
                                        ],
                                      )
                                    : Column(
                                        children: [
                                          Visibility(
                                            visible: controller
                                                    .actions[index].date !=
                                                controller
                                                    .actions[index - 1].date,
                                            child: ActionDateText(
                                              date: controller
                                                  .actions[index].date,
                                            ),
                                          ),
                                          ActionItem(
                                            action: controller.actions[index],
                                          ),
                                        ],
                                      ),
                              ],
                            );
                          },
                        ),
                      ),
                      SizedBox(height: SizeConfig().scaleHeight(30)),
                      AppElevatedButton(
                        title: 'See more',
                        onPressed: () {},
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
