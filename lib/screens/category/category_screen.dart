import 'package:budget_planner/extenssions/app_colors_extenssion.dart';
import 'package:budget_planner/getx_controllers/category_getx_controller.dart';
import 'package:budget_planner/getx_controllers/users_getx_controller.dart';
import 'package:budget_planner/models/category.dart';
import 'package:budget_planner/utils/enums.dart';
import 'package:budget_planner/utils/helpers.dart';
import 'package:budget_planner/utils/size_config.dart';
import 'package:budget_planner/widgets/app_elevated_button.dart';
import 'package:budget_planner/widgets/app_text_field.dart';
import 'package:budget_planner/widgets/category_type_item.dart';
import 'package:budget_planner/widgets/screen_header.dart';
import 'package:flutter/material.dart';

class CategoryScreen extends StatefulWidget {
  const CategoryScreen({Key? key}) : super(key: key);

  @override
  _CategoryScreenState createState() => _CategoryScreenState();
}

class _CategoryScreenState extends State<CategoryScreen> with Helpers {
  late TextEditingController _categoryNameTextController;
  CategoryType? _categoryType;
  bool _addEnabled = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _categoryNameTextController = TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _categoryNameTextController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: () => Navigator.pop(context),
            icon: Icon(Icons.close),
          ),
        ],
        automaticallyImplyLeading: false,
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: SizeConfig().scaleWidth(20),
          vertical: SizeConfig().scaleWidth(20),
        ),
        child: Column(
          children: [
            ScreenHeader(
              imageName: 'icon_wallet',
              title: 'Add Category',
              info: '',
            ),
            Row(
              children: [
                CategoryTypeItem(
                  onTap: () => changeCategoryType(CategoryType.Expense),
                  title: 'Expenses',
                  selected: _categoryType == CategoryType.Expense,
                  icon: Icons.arrow_upward,
                  iconColor: AppColors.red,
                ),
                SizedBox(width: SizeConfig().scaleWidth(10)),
                CategoryTypeItem(
                  onTap: () => changeCategoryType(CategoryType.Income),
                  title: 'Income',
                  selected: _categoryType == CategoryType.Income,
                  icon: Icons.arrow_downward,
                  iconColor: AppColors.green,
                ),
              ],
            ),
            SizedBox(height: SizeConfig().scaleHeight(11)),
            Container(
              width: double.infinity,
              height: SizeConfig().scaleHeight(60),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(9),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    offset: Offset(0, 0),
                    blurRadius: 6,
                    color: Colors.black.withOpacity(0.21),
                  ),
                ],
              ),
              child: AppTextField(
                textAlign: TextAlign.start,
                enablePadding: true,
                controller: _categoryNameTextController,
                hintText: 'Enter category name',
                keyboardType: TextInputType.text,
                onChanged: (String value) => validateForm(),
              ),
            ),
            SizedBox(height: SizeConfig().scaleHeight(30)),
            AppElevatedButton(
              title: 'Add',
              onPressed: () async => await performCreateCategory(),
              color: _addEnabled
                  ? AppColors.app_button_color
                  : AppColors.app_button_color_disabled,
            ),
          ],
        ),
      ),
    );
  }

  void validateForm() {
    updateEnableStatus(checkData());
  }

  void updateEnableStatus(bool status) {
    setState(() {
      _addEnabled = status;
    });
  }

  void changeCategoryType(CategoryType? categoryType) {
    setState(() {
      _categoryType = categoryType;
    });
    validateForm();
  }

  Future<void> performCreateCategory() async {
    if (checkData()) {
      await createCategory();
    }
  }

  bool checkData() {
    if (_categoryType != null && _categoryNameTextController.text.isNotEmpty) {
      return true;
    }
    return false;
  }

  Future<void> createCategory() async {
    bool created = await CategoryGetxController.to.create(category: category);
    String message = created
        ? 'Category saved successfully'
        : 'Failed to create new category!';
    showSnackBar(context: context, content: message, error: !created);
    if (created) clear();
  }

  Category get category {
    Category category = Category();
    category.name = _categoryNameTextController.text;
    category.expense = _categoryType == CategoryType.Expense;
    category.userId = UsersGetxController.to.user.id;
    return category;
  }

  void clear() {
    _categoryNameTextController.text = '';
    changeCategoryType(null);
  }
}
