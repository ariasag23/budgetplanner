import 'package:budget_planner/extenssions/app_colors_extenssion.dart';
import 'package:budget_planner/screens/out_boarding/out_boarding_content.dart';
import 'package:budget_planner/utils/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:get/get.dart';

class OutBoardingScreen extends StatefulWidget {
  const OutBoardingScreen({Key? key}) : super(key: key);

  @override
  _OutBoardingScreenState createState() => _OutBoardingScreenState();
}

class _OutBoardingScreenState extends State<OutBoardingScreen>
    with SingleTickerProviderStateMixin {
  late AnimationController controller;
  late PageController _pageController;
  int _currentPageIndex = 0;

  @override
  void initState() {
    _pageController = PageController();
    controller = AnimationController(
      vsync: this,
    )..addListener(() {});
    controller.value = 1 / 3;
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsetsDirectional.only(
              start: SizeConfig().scaleWidth(108),
              end: SizeConfig().scaleWidth(108),
              top: SizeConfig().scaleHeight(62),
            ),
            child: Container(
              clipBehavior: Clip.antiAlias,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                // color: AppColors.app_primary_light
              ),
              child: LinearProgressIndicator(
                value: controller.value,
                minHeight: SizeConfig().scaleHeight(10),
                valueColor: AlwaysStoppedAnimation(AppColors.app_button_color),
                backgroundColor: AppColors.app_primary_light,
                // backgroundColor: Colors.transparent,
              ),
            ),
          ),
          SizedBox(height: SizeConfig().scaleHeight(45)),
          Expanded(
            child: PageView(
              controller: _pageController,
              onPageChanged: (int currentPageIndex) {
                setState(() {
                  _currentPageIndex = currentPageIndex;
                  controller.value =
                      (currentPageIndex.toDouble() / 3) + (1 / 3);
                });
              },
              children: [
                OutBoardingContent(
                    image: 'out_boarding_1',
                    title: AppLocalizations.of(context)!.out_boarding_1_title,
                    content:
                    AppLocalizations.of(context)!.out_boarding_1_content),
                OutBoardingContent(
                    image: 'out_boarding_2',
                    title: AppLocalizations.of(context)!.out_boarding_2_title,
                    content:
                    AppLocalizations.of(context)!.out_boarding_2_content),
                OutBoardingContent(
                    image: 'out_boarding_3',
                    title: AppLocalizations.of(context)!.out_boarding_3_title,
                    content:
                    AppLocalizations.of(context)!.out_boarding_3_content),
              ],
            ),
          ),
          SizedBox(height: SizeConfig().scaleHeight(62)),
          Padding(
            padding:
            EdgeInsets.symmetric(horizontal: SizeConfig().scaleWidth(20)),
            child: Visibility(
              visible: _currentPageIndex != 2,
              replacement: ElevatedButton(
                onPressed: () => Navigator.pushReplacementNamed(context, '/login_screen'),
                child: Text('Start'),
                style: ElevatedButton.styleFrom(
                  primary: AppColors.app_button_color,
                  minimumSize:
                  Size(double.infinity, SizeConfig().scaleHeight(60)),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30),
                  ),
                ),
              ),
              child: ElevatedButton(
                onPressed: () {
                  _pageController.nextPage(
                      duration: Duration(seconds: 1), curve: Curves.easeIn);
                },
                child: Text('Next'),
                style: ElevatedButton.styleFrom(
                  primary: AppColors.app_button_color,
                  minimumSize:
                  Size(double.infinity, SizeConfig().scaleHeight(60)),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30),
                  ),
                ),
              ),
            ),
          ),
          SizedBox(height: SizeConfig().scaleHeight(17)),
          TextButton(
              onPressed: () => Navigator.pushReplacementNamed(context, '/login_screen'),
              child: Text('Skip')),
          SizedBox(height: SizeConfig().scaleHeight(44)),
        ],
      ),
    );
  }
}
