import 'package:budget_planner/utils/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class OutBoardingContent extends StatelessWidget {

  final String image;
  final String title;
  final String content;

  OutBoardingContent({
    required this.image,
    required this.title,
    required this.content,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: SizeConfig().scaleWidth(20)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(
            title,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: SizeConfig().scaleTextFont(20)),
          ),
          SizedBox(height: SizeConfig().scaleHeight(11)),
          Text(
            content,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: SizeConfig().scaleTextFont(15),
            ),
          ),
          SizedBox(height: SizeConfig().scaleHeight(55)),
          SvgPicture.asset('images/$image.svg'),
        ],
      ),
    );
  }
}
