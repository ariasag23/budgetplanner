import 'package:budget_planner/extenssions/app_colors_extenssion.dart';
import 'package:budget_planner/getx_controllers/currencies_getx_controller.dart';
import 'package:budget_planner/utils/size_config.dart';
import 'package:budget_planner/widgets/currency_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:get/get.dart';

class CurrencyScreen extends StatefulWidget {
  const CurrencyScreen({Key? key}) : super(key: key);

  @override
  _CurrencyScreenState createState() => _CurrencyScreenState();
}

class _CurrencyScreenState extends State<CurrencyScreen> {
  CurrencyGetxController controller = Get.put(CurrencyGetxController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          AppLocalizations.of(context)!.currency,
          style: TextStyle(
            color: AppColors.app_primary,
            fontWeight: FontWeight.bold,
          ),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        iconTheme: IconThemeData(color: AppColors.app_button_color),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => popFromCurrency(),
        ),
      ),
      body: Container(
        margin: EdgeInsetsDirectional.only(
          start: SizeConfig().scaleWidth(20),
          end: SizeConfig().scaleWidth(20),
          top: SizeConfig().scaleHeight(30),
        ),
        padding: EdgeInsetsDirectional.only(
          top: SizeConfig().scaleHeight(24),
          start: SizeConfig().scaleWidth(15),
          end: SizeConfig().scaleWidth(15),
        ),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
          boxShadow: [
            BoxShadow(
                offset: Offset(0, 10),
                blurRadius: 18,
                color: AppColors.shadow_color),
          ],
        ),
        child: GetX<CurrencyGetxController>(
          builder: (CurrencyGetxController controller) {
            return ListView.separated(
              shrinkWrap: true,
              itemCount: controller.currencies.length,
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (context, index) {
                return CurrencyItem(
                  name: controller.currencies[index].nameEn,
                  checked: controller.currencies[index].checked,
                  onPressed: () => controller.changeCheckStatus(index),
                );
              },
              separatorBuilder: (context, index) {
                return Divider(
                  thickness: 0.3,
                  height: 50,
                  color: AppColors.text_field_hint_color,
                );
              },
            );
          },
        ),
      ),
    );
  }

  void popFromCurrency() {
    Navigator.pop(context, CurrencyGetxController.to.getSelectedCurrency());
  }
}
