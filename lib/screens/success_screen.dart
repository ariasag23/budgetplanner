import 'package:budget_planner/extenssions/app_colors_extenssion.dart';
import 'package:budget_planner/utils/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SuccessScreen extends StatefulWidget {
  const SuccessScreen({Key? key}) : super(key: key);

  @override
  _SuccessScreenState createState() => _SuccessScreenState();
}

class _SuccessScreenState extends State<SuccessScreen> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration(seconds: 3),(){
      Navigator.pushNamedAndRemoveUntil(context, '/main_screen', (route) => false);
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              alignment: Alignment.center,
              margin: EdgeInsetsDirectional.only(
                bottom: SizeConfig().scaleHeight(29),
              ),
              width: SizeConfig().scaleWidth(120),
              height: SizeConfig().scaleHeight(115),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(25),
                boxShadow: [
                  BoxShadow(
                    offset: Offset(0, 4),
                    blurRadius: 4,
                    spreadRadius: 0,
                    color: Colors.black.withOpacity(0.25),
                  )
                ],
              ),
              child: SvgPicture.asset(
                'images/ic_like.svg',
                // 'images/app_icon.svg',
                fit: BoxFit.contain,
              ),
            ),
            Text(
              'Congratulations',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: AppColors.app_primary,
                fontSize: SizeConfig().scaleTextFont(15),
              ),
            ),
            SizedBox(height: SizeConfig().scaleHeight(12)),
            Text(
              'You have registered',
              // AppLocalizations.of(context)!.login_hint,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: AppColors.text_field_hint_color,
                fontSize: SizeConfig().scaleTextFont(15),
              ),
            )
          ],
        ),
      ),
    );
  }
}
