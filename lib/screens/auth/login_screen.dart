import 'package:budget_planner/extenssions/app_colors_extenssion.dart';
import 'package:budget_planner/getx_controllers/users_getx_controller.dart';
import 'package:budget_planner/utils/helpers.dart';
import 'package:budget_planner/utils/size_config.dart';
import 'package:budget_planner/widgets/screen_header.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:get/instance_manager.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> with Helpers {
  UsersGetxController _usersController = Get.put(UsersGetxController());
  late TextEditingController _emailTextController;
  late TextEditingController _pinTextController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _emailTextController = TextEditingController();
    _pinTextController = TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _emailTextController.dispose();
    _pinTextController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: Padding(
        padding: EdgeInsetsDirectional.only(
          start: SizeConfig().scaleWidth(20),
          end: SizeConfig().scaleWidth(20),
          top: SizeConfig().scaleHeight(86),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ScreenHeader(
              imageName: 'icon_wallet',
              title: AppLocalizations.of(context)!.login_title,
              info: AppLocalizations.of(context)!.login_hint,
            ),
            SizedBox(height: SizeConfig().scaleHeight(50)),
            DecoratedBox(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(9),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    offset: Offset(0, 0),
                    blurRadius: 6,
                    color: Colors.black.withOpacity(0.21),
                  )
                ],
              ),
              child: TextField(
                controller: _emailTextController,
                keyboardType: TextInputType.emailAddress,
                style: TextStyle(
                    fontSize: SizeConfig().scaleTextFont(15),
                    color: AppColors.text_field_hint_color,
                    fontFamily: 'Montserrat'),
                decoration: InputDecoration(
                  hintText: AppLocalizations.of(context)!.email_address,
                  contentPadding: EdgeInsets.symmetric(
                      horizontal: SizeConfig().scaleWidth(20)),
                  focusedBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                ),
              ),
            ),
            SizedBox(height: SizeConfig().scaleHeight(15)),
            DecoratedBox(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(9),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    offset: Offset(0, 0),
                    blurRadius: 6,
                    color: Colors.black.withOpacity(0.21),
                  )
                ],
              ),
              child: TextField(
                controller: _pinTextController,
                obscureText: true,
                maxLength: 4,
                keyboardType: TextInputType.text,
                style: TextStyle(
                  fontSize: SizeConfig().scaleTextFont(15),
                  color: AppColors.text_field_hint_color,
                  fontFamily: 'Montserrat',
                ),
                decoration: InputDecoration(
                  hintText: AppLocalizations.of(context)!.password,
                  counterText: '',
                  contentPadding: EdgeInsets.symmetric(
                    horizontal: SizeConfig().scaleWidth(20),
                  ),
                  focusedBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                ),
              ),
            ),
            SizedBox(height: SizeConfig().scaleHeight(30)),
            ElevatedButton(
              onPressed: () async {
                await login();
              },
              child: Text(AppLocalizations.of(context)!.login),
              style: ElevatedButton.styleFrom(
                primary: AppColors.app_button_color,
                minimumSize: Size(
                  double.infinity,
                  SizeConfig().scaleHeight(60),
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30),
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(AppLocalizations.of(context)!.no_account),
                TextButton(
                  onPressed: () =>
                      Navigator.pushNamed(context, '/create_account_screen'),
                  child: Text(AppLocalizations.of(context)!.create_now),
                  style: TextButton.styleFrom(padding: EdgeInsets.zero),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Future<void> performLogin() async {
    if (checkData()) {
      await login();
    }
  }

  bool checkData() {
    if (_emailTextController.text.isNotEmpty &&
        _pinTextController.text.isNotEmpty) {
      return true;
    }

    return false;
  }

  Future<void> login() async {
    bool status = await _usersController.login(
        email: _emailTextController.text, pin: _pinTextController.text);
    if (status) {
      Navigator.pushReplacementNamed(context, '/main_screen');
    } else {
      //SHOW MESSAGE
      showSnackBar(
          context: context,
          content: 'Login failed, check your credentials!',
          error: true);
    }
  }
}
