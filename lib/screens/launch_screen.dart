import 'package:budget_planner/extenssions/app_colors_extenssion.dart';
import 'package:budget_planner/getx_controllers/currencies_getx_controller.dart';
import 'package:budget_planner/getx_controllers/users_getx_controller.dart';
import 'package:budget_planner/preferences/user_preferences.dart';
import 'package:budget_planner/utils/size_config.dart';
import 'package:budget_planner/widgets/app_icon.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:get/get.dart';

class LaunchScreen extends StatefulWidget {
  const LaunchScreen({Key? key}) : super(key: key);

  @override
  _LaunchScreenState createState() => _LaunchScreenState();
}

class _LaunchScreenState extends State<LaunchScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Get.put(CurrencyGetxController());
    Get.put(UsersGetxController());
    Future.delayed(Duration(seconds: 3)).then((value) {
      String route = UserPreferences().loggedIn ? '/main_screen' : '/out_boarding_screen';
      Navigator.pushReplacementNamed(context, route);
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            AppIcon(),
            Text(
              AppLocalizations.of(context)!.app_name,
              style: TextStyle(
                fontSize: SizeConfig().scaleTextFont(24),
                fontWeight: FontWeight.bold,
                color: AppColors.app_primary,
                fontFamily: 'Montserrat',
              ),
            )
          ],
        ),
      ),
    );
  }
}