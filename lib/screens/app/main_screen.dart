import 'package:budget_planner/extenssions/app_colors_extenssion.dart';
import 'package:budget_planner/getx_controllers/actions_getx_controller.dart';
import 'package:budget_planner/getx_controllers/bn_getx_controller.dart';
import 'package:budget_planner/getx_controllers/category_getx_controller.dart';
import 'package:budget_planner/utils/size_config.dart';
import 'package:budget_planner/widgets/add_action_button.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {

  CategoryGetxController _categoryGetxController = Get.put(CategoryGetxController(),tag: 'CategoryGetxController');
  ActionsGetxController _actionsGetxController = Get.put(ActionsGetxController(),tag: 'ActionsGetxController');
  BnGetxController _bnGetxController = Get.put(BnGetxController(),tag: 'BnGetxController');

  @override
  Widget build(BuildContext context) {
    return GetX<BnGetxController>(
      tag: 'BnGetxController',
      builder: (BnGetxController controller) {
        return Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: controller.currentIndex.value != 3
              ? Colors.white
              : AppColors.app_primary_light,
          appBar: AppBar(
            elevation: 0,
            backgroundColor: controller.currentIndex.value != 3
                ? Colors.white
                : AppColors.app_primary_light,
            centerTitle: true,
            title: Text(controller.title),
            actions: [
              Visibility(
                visible: controller.index == 0,
                child: IconButton(
                  onPressed: () =>
                      Navigator.pushNamed(context, '/settings_screen'),
                  icon: Icon(Icons.settings),
                ),
              ),
              Visibility(
                visible: controller.index == 1,
                child: IconButton(
                  onPressed: () =>
                      Navigator.pushNamed(context, '/category_screen'),
                  icon: Icon(Icons.add_circle_outlined),
                ),
              ),
            ],
          ),
          body: controller.screen,
          bottomNavigationBar: BottomNavigationBar(
            onTap: (int currentIndex) {
              if (currentIndex != 2)
                controller.changeSelectedIndex(index: currentIndex);
            },
            currentIndex: controller.index,
            type: BottomNavigationBarType.fixed,
            selectedItemColor: AppColors.app_primary,
            selectedLabelStyle: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: SizeConfig().scaleWidth(10),
            ),
            unselectedItemColor: AppColors.un_selected_item_color,
            unselectedLabelStyle: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: SizeConfig().scaleWidth(10),
            ),
            items: [
              BottomNavigationBarItem(
                icon: Icon(Icons.insert_chart_outlined_outlined),
                label: 'Home',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.category),
                label: 'Categories',
              ),
              BottomNavigationBarItem(
                icon: AddActionButton(),
                label: '',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.person),
                label: 'Profile',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.error_outline),
                label: 'Tips',
              ),
            ],
          ),
        );
      },
    );
  }
}
