import 'package:budget_planner/extenssions/app_colors_extenssion.dart';
import 'package:budget_planner/utils/size_config.dart';
import 'package:budget_planner/widgets/app_icon.dart';
import 'package:flutter/material.dart';

class AboutScreen extends StatelessWidget {
  const AboutScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('About'),
      ),
      body: Stack(
        children: [
          Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                AppIcon(),
                Text(
                  'Budget Planner',
                  style: TextStyle(
                    fontSize: SizeConfig().scaleTextFont(24),
                    fontWeight: FontWeight.bold,
                    color: AppColors.app_primary,
                  ),
                ),
                SizedBox(height: SizeConfig().scaleHeight(15)),
                Text(
                  'GGateWay - Flutter',
                  style: TextStyle(
                    fontSize: SizeConfig().scaleTextFont(18),
                    color: AppColors.app_primary,
                    fontWeight: FontWeight.w300,
                  ),
                ),
                SizedBox(height: SizeConfig().scaleHeight(5)),
                Text(
                  'Student Name',
                  style: TextStyle(
                    fontSize: SizeConfig().scaleTextFont(15),
                    color: AppColors.app_primary.withOpacity(0.47),
                    fontWeight: FontWeight.w300,
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            bottom: SizeConfig().scaleHeight(20),
            left: 0,
            right: 0,
            child: Text(
              'V 1.0.0',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: SizeConfig().scaleTextFont(15),
              ),
            ),
          )
        ],
      ),
    );
  }
}
