import 'package:budget_planner/extenssions/app_colors_extenssion.dart';
import 'package:budget_planner/getx_controllers/actions_getx_controller.dart';
import 'package:budget_planner/getx_controllers/bn_getx_controller.dart';
import 'package:budget_planner/getx_controllers/category_getx_controller.dart';
import 'package:budget_planner/getx_controllers/currencies_getx_controller.dart';
import 'package:budget_planner/getx_controllers/users_getx_controller.dart';
import 'package:budget_planner/preferences/user_preferences.dart';
import 'package:budget_planner/utils/helpers.dart';
import 'package:budget_planner/utils/size_config.dart';
import 'package:budget_planner/widgets/settings_item.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SettingsScreen extends StatelessWidget with Helpers {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('Settings'),
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(
          vertical: SizeConfig().scaleHeight(40),
          horizontal: SizeConfig().scaleWidth(20),
        ),
        children: [
          Text(
            'General',
            style: TextStyle(
              color: AppColors.app_primary,
              fontWeight: FontWeight.w500,
              fontSize: SizeConfig().scaleTextFont(15),
            ),
          ),
          SettingsItem(
            marginTop: 15,
            title: 'About App',
            icon: Icons.info,
            onPressed: () => Navigator.pushNamed(context, '/about_screen'),
          ),
          SettingsItem(
            title: 'Language',
            icon: Icons.language,
            onPressed: () {},
          ),
          SettingsItem(
            title: 'Logout',
            icon: Icons.logout,
            onPressed: () async => await logout(context),
            marginBottom: 25,
          ),
          Text(
            'Accounts & data',
            style: TextStyle(
              color: AppColors.app_primary,
              fontWeight: FontWeight.w500,
              fontSize: SizeConfig().scaleTextFont(15),
            ),
          ),
          SettingsItem(
            title: 'Clear account data',
            icon: Icons.delete_forever,
            textColor: AppColors.red,
            iconColor: AppColors.red,
            onPressed: () async => await clearAccountData(context),
            marginTop: 15,
            marginBottom: 15,
            borderColor: AppColors.red,
          ),
          SettingsItem(
            title: 'Remove account',
            icon: Icons.person_remove_alt_1,
            textColor: Colors.white,
            iconColor: Colors.white,
            onPressed: () async => await removeUserAccount(context),
            marginBottom: 15,
            backgroundColor: AppColors.red,
          ),
        ],
      ),
    );
  }

  Future<void> clearAccountData(BuildContext context) async {
    bool cleared = await UsersGetxController.to.clearAccountData();
    String message =
        cleared ? 'User account data cleared' : 'Failed to clear account data';
    showSnackBar(context: context, content: message, error: !cleared);
  }

  Future<void> removeUserAccount(BuildContext context) async {
    bool removed = await UsersGetxController.to.removeUserAccount();
    String message = removed
        ? 'User account removed successfully'
        : 'Failed to remove user account';
    showSnackBar(context: context, content: message, error: !removed);
    if (removed) Navigator.pushReplacementNamed(context, '/login_screen');
  }

  Future<void> logout(BuildContext context) async {
    bool status = await UserPreferences().logout();
    if (status) {
      await Get.delete<CategoryGetxController>(tag: 'CategoryGetxController');
      await Get.delete<ActionsGetxController>(tag: 'ActionsGetxController');
      await Get.delete<BnGetxController>(tag: 'BnGetxController');

      CurrencyGetxController.to.undoCheckedCurrency();
      Navigator.pushReplacementNamed(context, '/login_screen');
    }
  }
}
