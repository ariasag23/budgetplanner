import 'package:budget_planner/extenssions/app_colors_extenssion.dart';
import 'package:budget_planner/getx_controllers/actions_getx_controller.dart';
import 'package:budget_planner/getx_controllers/category_getx_controller.dart';
import 'package:budget_planner/getx_controllers/currencies_getx_controller.dart';
import 'package:budget_planner/getx_controllers/users_getx_controller.dart';
import 'package:budget_planner/models/category.dart';
import 'package:budget_planner/models/currency.dart';
import 'package:budget_planner/models/user_action.dart';
import 'package:budget_planner/screens/bottom_navigation/categories_screen.dart';
import 'package:budget_planner/utils/enums.dart';
import 'package:budget_planner/utils/helpers.dart';
import 'package:budget_planner/utils/size_config.dart';
import 'package:budget_planner/widgets/app_elevated_button.dart';
import 'package:budget_planner/widgets/app_text_field.dart';
import 'package:budget_planner/widgets/category_type_item.dart';
import 'package:budget_planner/widgets/screen_header.dart';
import 'package:budget_planner/widgets/title_with_option_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:intl/intl.dart';

class AddActionScreen extends StatefulWidget {
  const AddActionScreen({Key? key}) : super(key: key);

  @override
  _AddActionScreenState createState() => _AddActionScreenState();
}

class _AddActionScreenState extends State<AddActionScreen> with Helpers {
  CategoryType? _categoryType;
  bool _addEnabled = false;
  Currency? _currency;
  Category? _category;

  DateTime? _pickedDateValue;
  String? _pickedDate;

  late TextEditingController _actionAmountTextController;
  late TextEditingController _noteTextController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _actionAmountTextController = TextEditingController();
    _noteTextController = TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _actionAmountTextController.dispose();
    _noteTextController.dispose();
    CategoryGetxController.to.undoCheckedCategory();
    CurrencyGetxController.to.undoCheckedCurrency();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: () => Navigator.pop(context),
            icon: Icon(Icons.close),
          ),
        ],
        automaticallyImplyLeading: false,
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: SizeConfig().scaleWidth(20),
        ),
        child: Column(
          children: [
            ScreenHeader(
              imageName: 'icon_wallet',
              title: 'Add Operation',
              info: '',
            ),
            TextField(
              controller: _actionAmountTextController,
              textAlign: TextAlign.center,
              keyboardType: TextInputType.number,
              maxLength: 8,
              onChanged: (String value) => validateForm(),
              style: TextStyle(
                color: AppColors.text_field_title_color,
                fontWeight: FontWeight.bold,
                fontSize: SizeConfig().scaleTextFont(21),
              ),
              decoration: InputDecoration(
                counterText: '',
                hintText: '\$ 0,00',
                hintStyle: TextStyle(
                  color: AppColors.text_field_title_color,
                  fontWeight: FontWeight.bold,
                  fontSize: SizeConfig().scaleTextFont(21),
                ),
                enabledBorder: InputBorder.none,
                focusedBorder: InputBorder.none,
              ),
            ),
            SizedBox(height: SizeConfig().scaleHeight(15)),
            Row(
              children: [
                CategoryTypeItem(
                  onTap: () {},
                  title: 'Expenses',
                  selected: _categoryType == CategoryType.Expense,
                  icon: Icons.arrow_upward,
                  iconColor: AppColors.red,
                ),
                SizedBox(width: SizeConfig().scaleWidth(10)),
                CategoryTypeItem(
                  onTap: () {},
                  title: 'Income',
                  selected: _categoryType == CategoryType.Income,
                  icon: Icons.arrow_downward,
                  iconColor: AppColors.green,
                ),
              ],
            ),
            SizedBox(height: SizeConfig().scaleHeight(11)),
            Container(
              margin: EdgeInsets.only(bottom: SizeConfig().scaleHeight(11)),
              padding: EdgeInsets.symmetric(
                horizontal: SizeConfig().scaleWidth(17),
                vertical: SizeConfig().scaleHeight(10),
              ),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(8),
                boxShadow: [
                  BoxShadow(
                    offset: Offset(0, 3),
                    color: Colors.black.withOpacity(0.25),
                    blurRadius: 6,
                  ),
                ],
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TitleWithOptionItem(
                    onPressed: () => navigateToCategoryScreen(),
                    title: AppLocalizations.of(context)!.category,
                    content: _category?.name,
                  ),
                  Divider(
                    thickness: 0.5,
                    color: AppColors.text_field_hint_color,
                  ),
                  TitleWithOptionItem(
                    onPressed: () => pickDate(),
                    title: AppLocalizations.of(context)!.date,
                    content: _pickedDate,
                  ),
                  Divider(
                    thickness: 0.5,
                    color: AppColors.text_field_hint_color,
                  ),
                  TitleWithOptionItem(
                    onPressed: () => navigateToCurrencyScreen(),
                    title: AppLocalizations.of(context)!.currency,
                    content: _currency?.nameEn,
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: SizeConfig().scaleWidth(14),
              ),
              height: SizeConfig().scaleHeight(112),
              margin: EdgeInsets.only(bottom: SizeConfig().scaleHeight(31)),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(8),
                boxShadow: [
                  BoxShadow(
                    offset: Offset(0, 3),
                    color: Colors.black.withOpacity(0.25),
                    blurRadius: 6,
                  ),
                ],
              ),
              child: AppTextField(
                textAlign: TextAlign.start,
                controller: _noteTextController,
                maxLength: 150,
                maxLines: 4,
                keyboardType: TextInputType.multiline,
                hintText: 'Note',
              ),
            ),
            AppElevatedButton(
              title: 'Add',
              onPressed: () async => await saveAction(),
              // onPressed: () async => await performCreateCategory(),
              color: _addEnabled
                  ? AppColors.app_button_color
                  : AppColors.app_button_color_disabled,
            ),
          ],
        ),
      ),
    );
  }

  Future pickDate() async {
    DateTime? dateTime = await showDatePicker(
      context: context,
      initialDate: _pickedDateValue ?? DateTime.now(),
      firstDate: DateTime(2021, 1, 1),
      lastDate: DateTime.now().add(Duration(days: 365)),
    );
    if (dateTime != null) {
      _pickedDateValue = dateTime;
      var format = DateFormat.yMd('en');
      setState(() {
        _pickedDate = format.format(dateTime);
      });
      validateForm();
    }
  }

  void navigateToCurrencyScreen() async {
    var currency =
        await Navigator.pushNamed(context, '/currency_screen') as Currency?;
    if (currency != null) {
      setState(() {
        _currency = currency;
      });
      validateForm();
    }
  }

  void navigateToCategoryScreen() async {
    var category = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => CategoriesScreen(withScaffold: true),
      ),
    ) as Category?;
    if (category != null) {
      setState(() {
        _category = category;
        changeCategoryType(
          _category!.expense ? CategoryType.Expense : CategoryType.Income,
        );
      });
      validateForm();
    }
  }

  void changeCategoryType(CategoryType? categoryType) {
    setState(() {
      _categoryType = categoryType;
    });
    validateForm();
  }

  void validateForm() {
    updateEnableStatus(checkData());
  }

  void updateEnableStatus(bool status) {
    setState(() {
      _addEnabled = status;
    });
  }

  bool checkData() {
    if (_actionAmountTextController.text.isNotEmpty &&
        _category != null &&
        _pickedDate != null &&
        _currency != null) {
      return true;
    }
    return false;
  }

  Future<void> saveAction() async {
    if (_addEnabled) {
      bool created = await ActionsGetxController.to.create(action: action);
      String message =
          created ? 'Action saved successfully' : 'Failed to save new action!';
      showSnackBar(context: context, content: message, error: !created);
      if (created) clear();
    }
  }

  UserAction get action {
    UserAction action = UserAction();
    action.amount = double.parse(_actionAmountTextController.text);
    action.expense = _category!.expense;
    action.date = _pickedDateValue!;
    action.userId = UsersGetxController.to.user.id;
    action.currencyId = _currency!.id;
    action.categoryId = _category!.id;
    action.notes = _noteTextController.text;
    return action;
  }

  void clear() {
    CategoryGetxController.to.undoCheckedCategory();
    CurrencyGetxController.to.undoCheckedCurrency();
    _actionAmountTextController.text = '';
    _noteTextController.text = '';
    setState(() {
      _pickedDateValue = null;
      _pickedDate = null;
      _categoryType = null;
      _category = null;
      _currency = null;
    });
  }
}
