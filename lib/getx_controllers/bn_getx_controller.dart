import 'package:budget_planner/models/bn_screen.dart';
import 'package:budget_planner/screens/bottom_navigation/categories_screen.dart';
import 'package:budget_planner/screens/bottom_navigation/home_screen.dart';
import 'package:budget_planner/screens/bottom_navigation/profile_screen.dart';
import 'package:budget_planner/screens/bottom_navigation/tips_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class BnGetxController extends GetxController {
  List<BnScreen> _screens = <BnScreen>[];
  RxInt currentIndex = 0.obs;

  static BnGetxController get to => Get.find();

  void setupScreens() {
    _screens.add(BnScreen(title: 'Home', widget: HomeScreen()));
    _screens.add(BnScreen(title: 'Categories', widget: CategoriesScreen()));
    _screens.add(BnScreen(title: 'Profile', widget: ProfileScreen()));
    _screens.add(BnScreen(title: 'Tips', widget: TipsScreen()));
  }

  @override
  void onInit() {
    // TODO: implement onInit
    setupScreens();
    super.onInit();
  }

  void changeSelectedIndex({required int index}) {
    currentIndex.value = index > 1 ? index - 1 : index;
    currentIndex.refresh();
  }

  Widget get screen => _screens[currentIndex.value].widget;

  String get title => _screens[currentIndex.value].title;

  int get index =>  currentIndex > 1 ? currentIndex.value + 1 : currentIndex.value;
}
