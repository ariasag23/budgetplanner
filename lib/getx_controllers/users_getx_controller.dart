import 'package:budget_planner/getx_controllers/actions_getx_controller.dart';
import 'package:budget_planner/getx_controllers/category_getx_controller.dart';
import 'package:budget_planner/models/user.dart';
import 'package:budget_planner/preferences/user_preferences.dart';
import 'package:budget_planner/storage/controllers/user_db_controller.dart';
import 'package:get/get.dart';

class UsersGetxController extends GetxController {
  UserDbController _userDbController = UserDbController();
  late User user;

  static UsersGetxController get to => Get.find();

  @override
  void onInit() {
    if (UserPreferences().loggedIn) user = UserPreferences().user;
    super.onInit();
  }

  Future<bool> login({required String email, required String pin}) async {
    User? user = await _userDbController.login(email: email, pin: pin);
    if (user != null) {
      await UserPreferences().save(user: user);
      this.user = user;
      return true;
    }
    return false;
  }

  Future<bool> createAccount({required User user}) async {
    var newUserId = await _userDbController.createAccount(user: user);
    if (newUserId != 0) {
      user.id = newUserId;
      await UserPreferences().save(user: user);
      this.user = user;
      return true;
    }
    return false;
  }

  Future<bool> removeUserAccount() async {
    return await _userDbController.delete(
        userId: UsersGetxController.to.user.id);
  }

  Future<bool> clearAccountData() async {
    return await ActionsGetxController.to.deleteUserActions() &&
        await CategoryGetxController.to.deleteUserCategories();
  }

  Future<bool> removeAccount() async {
    return await ActionsGetxController.to.deleteUserActions() &&
        await CategoryGetxController.to.deleteUserCategories() &&
        await UsersGetxController.to.removeUserAccount() &&
        await UserPreferences().logout();
  }
}
